# system modules
import functools

# internal modules

# external modules
import scipy.signal
import numpy as np


def find_conspicuous_values(x, rel_prominence=None, **kwargs):
    """
    Find unusual peaks in the distribution of (integer!) values in x

    Args:
        x (numpy.ndarray of int): an array of integer values
        rel_prominence (float, optional): threshold for the ratio
            of peak prominence to the value of the peak, which is a loose proxy
            for the ratio to the neighbouring samples. Small numbers filter
            for high peak prominances, high numbers filter for small peak
            prominences.
    """
    # count how often every integer value appears
    # make sure the histogram is 0 outside: extend by 3:
    # +1 because in np.arange the upper bound is exclusive,
    # +1 for the rightmost element to be recognized as a peak, it's right neighbour must be 0
    # and another +1 because??? it doesn't work otherwise...
    count, edges = np.histogram(
        x,
        bins=np.arange(x.min() - 3, x.max() + 3, 1),
    )
    find_peaks_kwargs = kwargs.copy()
    find_peaks_kwargs.setdefault("width", 1)
    find_peaks_kwargs.setdefault("rel_height", 1)
    if rel_prominence is not None:
        find_peaks_kwargs.update(prominence=count * rel_prominence)
    peak_positions, peak_properties = scipy.signal.find_peaks(
        count, **find_peaks_kwargs
    )
    return edges[:-1][peak_positions], count[peak_positions]


def find_values(array, values):
    """
    Return a boolean mask indicating where a given ``array`` contains certain ``values``.

    Args:
        array (array-like): the array to search for position of values
        values (sequence): the values to look for
    """
    return functools.reduce(
        np.logical_or, map(lambda a: np.asarray(array) == a, values)
    )
